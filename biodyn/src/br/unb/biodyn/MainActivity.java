package br.unb.biodyn;

import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import br.unb.biodyn.model.LogFormat;
import br.unb.biodyn.ui.CounterUI;
import br.unb.biodyn.ui.LimitsWeightUI;
import br.unb.biodyn.ui.TimerUI;

public class MainActivity extends Activity {

	/********************************** CONSTANTS ***********************************/

	// Intent request code for result checking
	private static final int REQUEST_CONNECT_DEVICE = 1;
	private static final int REQUEST_ENABLE_BT = 2;

	/********************************** CLASS FIELDS ***********************************/

	private String mConnectedDeviceName = null;
	private BluetoothAdapter mBluetoothAdapter = null;
	private mReceiver myReceiver;
	private LimitsWeightUI limitsUI;
	private TimerUI restTimerUI;
	private CounterUI repetitionsUI;
	private CounterUI seriesUI;


	/********************************** LIFECYCLE METHODS ***********************************/

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		LogFormat.setLogMessage(LogFormat.MAIN_ACTIVITY, "+++ ON CREATE +++");
		setContentView(R.layout.main);

		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

		if (mBluetoothAdapter == null) {
			Toast.makeText(this, "Bluetooth is not available on this device!", Toast.LENGTH_LONG).show();
			this.finish();
			return;
		}

		limitsUI = (LimitsWeightUI) findViewById(R.id.limits);
		restTimerUI = (TimerUI) findViewById(R.id.timer);
		repetitionsUI = (CounterUI) findViewById(R.id.repetition_counter);
		seriesUI = (CounterUI) findViewById(R.id.series_counter);

		myReceiver = new mReceiver();
		IntentFilter filter = new IntentFilter();
		filter.addAction(BluetoothService.ACTION_DATA_RECEIVED);
		filter.addAction(BluetoothService.ACTION_MESSAGE_DEVICE_NAME);
		filter.addAction(BluetoothService.ACTION_MESSAGE_SHOW_TOAST);
		filter.addAction(Communicator.UIActions.ACTION_EXERCISE_FINISHED);
		filter.addAction(Communicator.UIActions.ACTION_START_REST_TIMER);
		filter.addAction(Communicator.UIActions.ACTION_STOP_REST_TIMER);
		filter.addAction(Communicator.UIActions.ACTION_UPDATE_REPETITIONS);
		filter.addAction(Communicator.UIActions.ACTION_UPDATE_SERIES);
		filter.addAction(Communicator.UIActions.ACTION_UPDATE_LIMITS);
		filter.addAction(Communicator.BioFeedbackServiceActions.ACTION_START);
		filter.addAction(Communicator.BioFeedbackServiceActions.ACTION_STOP);

		LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(this);
		localBroadcastManager.registerReceiver(myReceiver, filter);

		Communicator.initialize(localBroadcastManager);

		Intent intent = new Intent(ExerciseService.ACTION_EXERCISE_SERVICE);
		intent.putExtra(ExerciseService.EXERCISE_TYPE, ExerciseService.DYNAMIC_EXERCISE);
		startService(intent);
		
		Intent intent2 = new Intent(BioFeedbackService.ACTION_BIO_FEEDBACK_SERVICE);
		startService(intent2);

	}

	@Override
	public synchronized void onResume() {
		LogFormat.setLogMessage(LogFormat.MAIN_ACTIVITY, "+ ON RESUME +");
		super.onResume();
	}

	@Override
	public void onDestroy() {
		LocalBroadcastManager.getInstance(this).unregisterReceiver(myReceiver);
		LogFormat.setLogMessage(LogFormat.MAIN_ACTIVITY, "--- ON DESTROY ---");
		super.onDestroy();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.option_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.scan:
				enableBlueTooth();
				// Launch the DeviceListAncctivity to see devices and do scan
				Intent serverIntent = new Intent(this, DeviceListActivity.class);
				startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE);
				return true;
			case R.id.discoverable:
				ensureDiscoverable();
				return true;
		}
		return false;
	}

	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		LogFormat.setLogMessage(LogFormat.MAIN_ACTIVITY, "onActivityResult " + resultCode);
		switch (requestCode) {
			case REQUEST_CONNECT_DEVICE:
				if (resultCode == Activity.RESULT_OK) {
					String address = data.getExtras().getString(DeviceListActivity.EXTRA_DEVICE_ADDRESS);
					BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
					try {
						Intent intent = new Intent(BluetoothService.ACTION_CONNECT_TO_DEVICE);
						intent.putExtra(BluetoothService.KEY_DEVICE_TO_CONNECT, device);
						LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
					} catch (Exception e) {
						LogFormat.setLogMessageException(LogFormat.MAIN_ACTIVITY, "onActivityResult got an error!", e);
					}
				}
				break;
			case REQUEST_ENABLE_BT:
				if (resultCode == Activity.RESULT_OK) {
					Intent intent = new Intent(BluetoothService.ACTION_BLUETOOTH_SERVICE);
					startService(intent);
				} else {
					// User did not enable Bluetooth or an error occured
					LogFormat.setLogMessage(LogFormat.MAIN_ACTIVITY, "BlueTooth not enabled " + resultCode);
					
					Toast.makeText(this, R.string.bt_not_enabled_leaving, Toast.LENGTH_SHORT).show();
				}
		}
	}

	/********************************** OTHER METHODS ***********************************/

	// TODO remove this method and its menu button when Bluetooth communication
	// is done
	private void ensureDiscoverable() {
		
		LogFormat.setLogMessage(LogFormat.MAIN_ACTIVITY, "ensure discoverable ");

		if (mBluetoothAdapter.getScanMode() != BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) {
			Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
			discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
			startActivity(discoverableIntent);
		}
	}

	private void enableBlueTooth() {
		if (!mBluetoothAdapter.isEnabled()) { // Enable bluetooth and start
												// bluetooth service
			Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
			startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
		} else { // Bluetooth already enabled, just start the service
			Intent intent = new Intent(BluetoothService.ACTION_BLUETOOTH_SERVICE);
			startService(intent);
		}
	}

	public void onClick(View view) { // TODO relocate button logic (extract from
										// here)
		if (view.getId() == R.id.test_button) {// TODO remove when not necessary
			Communicator.getInstance().sendToExerciseService(Communicator.FLAG_FORCE_REPETITION,null);
		}
		if (view.getId() == R.id.reset_button) {
			Communicator.getInstance().sendToExerciseService(Communicator.FLAG_TOOGLE_RESET,null);
		}
		if (view.getId() == R.id.send_weight) {
			CounterUI counterUI = (CounterUI) findViewById(R.id.weight_counter);
			Intent intent = new Intent(BluetoothService.ACTION_DATA_RECEIVED);
			
			LogFormat.setLogMessage(LogFormat.MAIN_ACTIVITY, "counterUI value: " + counterUI.getValue());
						
			intent.putExtra(BluetoothService.KEY_DATA_RECEIVED, (float) counterUI.getValue());
			LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
		}
	}

	/********************************** PRIVATE CLASSES ***********************************/

	private class mReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();

			if (action.equals(BluetoothService.ACTION_DATA_RECEIVED)) {
				TextView test = (TextView) findViewById(R.id.text_real_weight);
				test.setText(String.valueOf(intent.getExtras().getFloat(BluetoothService.KEY_DATA_RECEIVED)));
			} else if (action.equals(BluetoothService.ACTION_MESSAGE_DEVICE_NAME)) {
				// save the connected device's name
				mConnectedDeviceName = intent.getExtras().getString(BluetoothService.KEY_DEVICE_NAME);
				Toast.makeText(getApplicationContext(), "Connected to " + mConnectedDeviceName, Toast.LENGTH_SHORT)
						.show();
			} else if (action.equals(BluetoothService.ACTION_MESSAGE_SHOW_TOAST)) {
				Toast.makeText(getApplicationContext(),
						intent.getExtras().getString(BluetoothService.KEY_TOAST_MESSAGE), Toast.LENGTH_SHORT).show();
			} else if (action.equals(Communicator.UIActions.ACTION_EXERCISE_FINISHED)) {
				exemplo_layout();
				//Toast.makeText(getApplicationContext(), "Finished", Toast.LENGTH_SHORT).show();
				restTimerUI.stopTimer();
			} else if (action.equals(Communicator.UIActions.ACTION_START_REST_TIMER))
				restTimerUI.startTimer();
			else if (action.equals(Communicator.UIActions.ACTION_STOP_REST_TIMER))
				restTimerUI.stopTimer();
			else if (action.equals(Communicator.UIActions.ACTION_UPDATE_REPETITIONS))
				repetitionsUI.setValue(intent.getExtras().getInt(Communicator.KEY_REPETITIONS));
			else if (action.equals(Communicator.UIActions.ACTION_UPDATE_SERIES))
				seriesUI.setValue(intent.getExtras().getInt(Communicator.KEY_SERIES));
			else if (action.equals(Communicator.UIActions.ACTION_UPDATE_LIMITS)) {
				limitsUI.getTopCounter().setValue((int) intent.getExtras().getFloat(Communicator.KEY_TOP_LIMIT));
				limitsUI.getBottomCounter().setValue((int) intent.getExtras().getFloat(Communicator.KEY_BOTTOM_LIMIT));
			}
		}

	}
	
    private void exemplo_layout() {
        //LayoutInflater é utilizado para inflar nosso layout em uma view.
        //-pegamos nossa instancia da classe
        LayoutInflater li = getLayoutInflater();
        
        //inflamos o layout alerta.xml na view
        View view = li.inflate(R.layout.alert_dialog_finished_exercise, null);
        //definimos para o botão do layout um clickListener
                
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Parabéns! Concluiu o exercício!");
        builder.setView(view);
        AlertDialog alerta = builder.create();
        alerta.show();
    } 
	
	
	
}