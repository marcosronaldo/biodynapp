package br.unb.biodyn.model;

import android.content.Context;
import android.os.Vibrator;

public class VibratoryBioFeedBack implements BioFeedback {
	
	private Vibrator vibrator;

	public VibratoryBioFeedBack(Context context) {
		this.vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
	}

	@Override
	public void start(long time) {
		this.vibrator.vibrate(time);
	}

	@Override
	public void stop() {
		this.vibrator.cancel();
	}

}
