package br.unb.biodyn.model;

public class ExerciseData {
	
	protected float maximumWeight;
	protected float minimumWeight;
	protected int repetitions;
	protected int series;
	
	public ExerciseData(float maximumWeight, float minimumWeight, int repetitions, int series) {
		this.maximumWeight = maximumWeight;
		this.minimumWeight = minimumWeight;
		this.repetitions = repetitions;
		this.series = series;
	}
	public ExerciseData() {
		this(0, 0, 0, 0);
	}
	
	public float getMaximumWeight() {
		return maximumWeight;
	}
	public void setMaximumWeight(float maximumWeight) {
		this.maximumWeight = maximumWeight;
	}
	public float getMinimumWeight() {
		return minimumWeight;
	}
	public void setMinimumWeight(float minimumWeight) {
		this.minimumWeight = minimumWeight;
	}
	public int getRepetitions() {
		return repetitions;
	}
	public void setRepetitions(int repetitions) {
		this.repetitions = repetitions;
	}
	public int getSeries() {
		return series;
	}
	public void setSeries(int series) {
		this.series = series;
	}
}
