package br.unb.biodyn.model;

public class FormatTime {
	
	private static final int MILLISECONDS_TO_MINUTES = 60000;
	private static final int MILLISECONDS_TO_SECONDS = 1000;
	private static final int FORMAT_SECONDS = 10;
	private static final int SECONDS_DIGITS = 2;
	private Long minutes;
	private Long seconds;
	private String totalTime;	
	
	public FormatTime() {
	}
	
	public String formatTimeMinutesAndSeconds(long duration) {
		this.minutes = duration/MILLISECONDS_TO_MINUTES;
		this.seconds = (duration%MILLISECONDS_TO_MINUTES)/MILLISECONDS_TO_SECONDS;
		if(seconds < FORMAT_SECONDS) {
			totalTime = this.minutes.toString()+":0"+this.seconds.toString();
		}
		else {
			totalTime = this.minutes.toString()+":"+this.seconds.toString();				
		}
		return totalTime;		
	}

	public String formatTimeSeconds(long duration) {
		String text = "";
		this.seconds = duration/MILLISECONDS_TO_SECONDS;
		if(seconds < Math.pow(10, SECONDS_DIGITS)) {
			for(int i = 0; i < SECONDS_DIGITS - this.seconds.toString().length() + 1; i++) {
				text += "0";
			}
		}
		text += this.seconds.toString();
		return text;
	}
}
