package br.unb.biodyn.ui;

import android.content.Context;
import android.util.AttributeSet;
import br.unb.biodyn.Communicator;
import br.unb.biodyn.model.ExerciseData;

public class CounterRepetitionUI extends CounterUI {

	public CounterRepetitionUI(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	@Override
	public void increase() {
		super.increase();
		sendUpdateRepetition();
	}
	
	@Override
	public void decrease() {
		super.decrease();
		sendUpdateRepetition();
	}
	
	private void sendUpdateRepetition() {
		ExerciseData exerciseData = new ExerciseData();
		exerciseData.setRepetitions(getValue());
		Communicator.getInstance().sendToExerciseService(Communicator.FLAG_REPETITIONS, exerciseData);
	}

}
