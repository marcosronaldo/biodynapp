package br.unb.biodyn;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import br.unb.biodyn.model.DynamicExercise;
import br.unb.biodyn.model.Exercise;
import br.unb.biodyn.model.IsometricExercise;

public class ExerciseService extends Service {

	/********************************** CONSTANTS ***********************************/

	public static final String ACTION_EXERCISE_SERVICE = "br.unb.biodyn.ExerciseService";
	public static final short DYNAMIC_EXERCISE = 0;
	public static final short ISOMETRIC_EXERCISE = 1;
	public static final String EXERCISE_TYPE = "exercise_type";

	/********************************** CLASS FIELDS ***********************************/

	private Exercise currentExercise;
	private DynamicExercise dynamicExercise;
	private IsometricExercise isometricExercise;
	private MyReceiver myReceiver;

	/********************************** LIFECYCLE METHODS ***********************************/

	@Override
	public void onCreate() {
		myReceiver = new MyReceiver();
		IntentFilter filter = new IntentFilter();
		filter.addAction(Communicator.ExerciseServiceActions.ACTION_TOGGLE_RESET);
		filter.addAction(Communicator.ExerciseServiceActions.ACTION_UPDATE_LIMITS);
		filter.addAction(Communicator.ExerciseServiceActions.ACTION_UPDATE_REPETITIONS);
		filter.addAction(Communicator.ExerciseServiceActions.ACTION_UPDATE_SERIES);
		filter.addAction(Communicator.ExerciseServiceActions.ACTION_FORCE_REPETITION);
		filter.addAction(BluetoothService.ACTION_DATA_RECEIVED);
		LocalBroadcastManager.getInstance(this).registerReceiver(myReceiver,
				filter);
		super.onCreate();
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		short type = intent.getExtras().getShort(EXERCISE_TYPE);
		switch (type) { // TODO: check if current is null
		case DYNAMIC_EXERCISE:
			if (dynamicExercise == null)
				dynamicExercise = new DynamicExercise();
			currentExercise = dynamicExercise;
			break;
		case ISOMETRIC_EXERCISE:
			if (isometricExercise == null)
				isometricExercise = new IsometricExercise();
			currentExercise = isometricExercise;
			break;
		}
		return super.onStartCommand(intent, flags, startId);
	}

	@Override
	public void onDestroy() {
		LocalBroadcastManager.getInstance(this).unregisterReceiver(myReceiver);
		super.onDestroy();
	}

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	/********************************** PRIVATE CLASSES ***********************************/

	private class MyReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			if (action.equals(BluetoothService.ACTION_DATA_RECEIVED)) {
				float floatValue = intent.getExtras().getFloat(BluetoothService.KEY_DATA_RECEIVED);
				currentExercise.computeData(floatValue);
			} else if (action
					.equals(Communicator.ExerciseServiceActions.ACTION_TOGGLE_RESET)) {
				currentExercise.reset();
			} else if (action
					.equals(Communicator.ExerciseServiceActions.ACTION_UPDATE_LIMITS)) {
				int top = intent.getExtras().getInt(Communicator.KEY_TOP_LIMIT);
				int bottom = intent.getExtras().getInt(
						Communicator.KEY_BOTTOM_LIMIT);
				currentExercise.setMaximumWeight(top);
				currentExercise.setMinimumWeight(bottom);
			} else if (action
					.equals(Communicator.ExerciseServiceActions.ACTION_UPDATE_REPETITIONS))
				currentExercise.setRepetitions(intent.getExtras().getInt(
						Communicator.KEY_REPETITIONS));
			else if (action
					.equals(Communicator.ExerciseServiceActions.ACTION_UPDATE_SERIES))
				currentExercise.setSeries(intent.getExtras().getInt(
						Communicator.KEY_SERIES));
			else if (action
					.equals(Communicator.ExerciseServiceActions.ACTION_FORCE_REPETITION)) // test
				currentExercise.countDownRepetitions();
		}
	}
}